:toc: right

= Rainbow Rocket

:sectnums:

image::resources/rr.png[width=100px]

:base: http://localhost:8000/gen
:cmd: rr

`{cmd}`: a utility to generate and optionnaly serve test images.

== Install

    https://gitlab.com/wilfried.kopp/rainbow_rocket

== Usage

If you run `{cmd}` without command, it will show you its help by default.

    ROCKET_ENV=dev ROCKET_PORT=8007 rr serve

NOTE: All the ROCKET ENV vars are supported.

== Image generation

You can control the output image using the following query params.
- format: jpg | png
- crosshair: true | false
- border: 0=None  1..N=width of the border
- pattern: see below

For all of the samples below, you can mix and match the following query params:
- w: width of the image
- h: height of the image

=== Patterns

You can produce various target images using one of the available patterns.
You can use any of the number `x` showing up as a `Some(x)` below.

The following for instance produces a 16x16 checkerboard:

{base}?border=0&pattern=52

----
include::src/routes/gen.rs[tag=patterns]
----

=== Samples

First start `{cmd}`: 

    rr serve

Then try the links below:

- Black image: {base}?border=0&pattern=0&crosshair=true 

image::doc/pattern_0.png[]

- White image: {base}?border=0&pattern=1&crosshair=true

image::doc/pattern_1.png[]

- Red image: {base}?border=0&pattern=2&crosshair=true

image::doc/pattern_2.png[]

- Green image: {base}?border=0&pattern=3&crosshair=true

image::doc/pattern_3.png[]

- Blue image: {base}?border=0&pattern=4&crosshair=true

image::doc/pattern_4.png[]

- Tiles 10x10 grid: {base}?border=0&pattern=10

image::doc/pattern_10.png[]

- Tiles 16x16 {base}?border=0&pattern=11

image::doc/pattern_11.png[]

- Noise B on W: {base}?border=0&pattern=20&crosshair=true

image::doc/pattern_20.png[]

- Noise Red on black {base}?border=0&pattern=21&crosshair=true

image::doc/pattern_21.png[]

- Noise blue on white {base}?border=0&pattern=22&crosshair=true

image::doc/pattern_22.png[]

- Random Gradient {base}?border=0&pattern=30&crosshair=true

image::doc/pattern_30.png[]

- Grid 10x10 {base}?border=0&pattern=40

image::doc/pattern_40.png[]

- Grid 16x16 {base}?border=0&pattern=41

image::doc/pattern_41.png[]

- Grid 16x8 {base}?border=0&pattern=42

image::doc/pattern_42.png[]

- Checkerboard 1,1 {base}?border=0&pattern=50&format=bmp&w=200&h=200

image::doc/pattern_50.png[]

- Checkerboard 10,10 {base}?border=0&pattern=51

image::doc/pattern_51.png[]

- Checkerboard 16,16 {base}?border=0&pattern=52

image::doc/pattern_52.png[]

- Checkerboard 32,32 {base}?border=0&pattern=53

image::doc/pattern_53.png[]

- Checkerboard 64,64 {base}?border=0&pattern=54

image::doc/pattern_54.png[]
