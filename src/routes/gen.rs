use crate::{
    image_gen::get_image,
    image_options::{ImageOptions, Pattern},
};
use rocket::{get, response::NamedFile};
use std::env;

#[get("/?<crosshair>&<w>&<h>&<format>&<border>&<pattern>", rank = 1)]
pub fn gen_sized(
    w: Option<u32>,
    h: Option<u32>,
    crosshair: Option<bool>,
    format: Option<String>,
    border: Option<u8>,
    pattern: Option<u8>,
) -> NamedFile {
    let size = match (w, h) {
        (Some(w), Some(h)) => (w, h),
        (Some(a), None) => (a, a * 3 / 4),
        (None, Some(a)) => (a * 4 / 3, a),
        _ => (800, 600),
    };
    let format = format.unwrap_or(String::from("jpg"));

    // tag::patterns[]
    let pattern = match pattern {
        Some(0) => Pattern::Uniform(0, 0, 0),
        Some(1) => Pattern::Uniform(255, 255, 255),
        Some(2) => Pattern::Uniform(255, 0, 0),
        Some(3) | None => Pattern::Uniform(0, 255, 0),
        Some(4) => Pattern::Uniform(0, 0, 255),
        Some(10) => Pattern::Tiles(10, 10),
        Some(11) => Pattern::Tiles(16, 16),
        Some(20) => Pattern::Noise(0, 0, 0, 255, 255, 255), // black on white
        Some(21) => Pattern::Noise(255, 0, 0, 0, 0, 0),     // red on black
        Some(22) => Pattern::Noise(0, 0, 255, 255, 255, 255), // blue on white
        Some(30) => Pattern::Gradient(16),
        Some(40) => Pattern::Grid(10, 10),
        Some(41) => Pattern::Grid(16, 16),
        Some(42) => Pattern::Grid(16, 8),
        Some(50) => Pattern::Checkerboard(1, 1),
        Some(51) => Pattern::Checkerboard(10, 10),
        Some(52) => Pattern::Checkerboard(16, 16),
        Some(53) => Pattern::Checkerboard(32, 32),
        Some(54) => Pattern::Checkerboard(64, 64),

        _ => unreachable!(),
    };
    // end::patterns[]

    let options = ImageOptions {
        size,
        crosshair: crosshair.unwrap_or_default(),
        format: format.clone(),
        border: border.unwrap_or(3),
        pattern,
        ..ImageOptions::default()
    };

    let image = get_image(Some(options));
    let image_name = "output";
    let tmp_folder = env::temp_dir();
    let tmpfile = format!(
        "{}{}.{}",
        tmp_folder.to_str().unwrap_or("tmp"),
        image_name,
        format
    );
    image.save(&tmpfile).unwrap();
    let file = NamedFile::open(tmpfile);
    
    file.unwrap()
}
