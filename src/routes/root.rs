
use rocket::get;

#[get("/")]
pub fn index() -> &'static str {
    r#"This small webservice generates images."#
}
