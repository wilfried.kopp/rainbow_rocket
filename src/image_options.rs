#[derive(Debug)]
pub enum Pattern {
    Uniform(u8, u8, u8),
    Checkerboard(u8, u8),
    Tiles(u8, u8),
    Grid(u8, u8),
    Noise(u8, u8, u8, u8, u8, u8),
    Gradient(u8),
}

#[derive(Debug)]
pub struct ImageOptions {
    pub size: (u32, u32),
    pub crosshair: bool,
    pub format: String,
    pub border: u8,
    pub pattern: Pattern,
}

impl Default for ImageOptions {
    fn default() -> Self {
        Self {
            size: (800_u32, 600_u32),
            crosshair: true,
            format: String::from("jpg"),
            border: 3,
            pattern: Pattern::Checkerboard(16, 16),
        }
    }
}
