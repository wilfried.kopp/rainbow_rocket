#![feature(proc_macro_hygiene, decl_macro)]
use clap::{crate_version, App, AppSettings};
use rocket::{self, routes};

mod image_options;
mod routes;
mod image_gen;

fn main() {
    let app = App::new("rr: Rainbow Rocket");
    let matches = &app
        .setting(AppSettings::ArgRequiredElseHelp)
        .version(crate_version!())
        .author("Wilfried Kopp <chevdor@gmail.com>")
        .about("A web-service that generates images.")
        .subcommand(App::new("serve").about("Serves over http"))
        .get_matches();

    if let Some(ref _matches) = matches.subcommand_matches("serve") {
        rocket::ignite()
            .mount("/", routes![routes::root::index])
            .mount("/gen", routes![routes::gen::gen_sized])
            .launch();
    }
}

#[cfg(test)]
mod tests {
   
}
