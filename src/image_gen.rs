use crate::image_options::{ImageOptions, Pattern};
use image::{ImageBuffer, Rgb, RgbImage};
use rand::prelude::*;
use std::convert::TryFrom;

pub fn get_image(opts: Option<ImageOptions>) -> RgbImage {
    let options = opts.unwrap_or_default();
    println!("{:#?}", options);

    let (width, height) = options.size;
    let mut imgbuf = ImageBuffer::<Rgb<u8>, Vec<u8>>::new(width, height);

    // TODO: move that out...
    let mut rng = rand::thread_rng();
    let random: f32 = rng.gen();
    let mut mean_lumi = 0u16;

    match options.pattern {
        Pattern::Uniform(r, g, b) => {
            mean_lumi = r as u16 + g as u16 + b as u16;
            for (_x, _y, pixel) in imgbuf.enumerate_pixels_mut() {
                *pixel = image::Rgb([r, g, b]);
            }
        }

        Pattern::Tiles(a, b) => {
            let mut total = 0;
            let mut bright = 0;

            for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
                total += 1;
                if (x / a as u32) % 2 == 0 && (y / b as u32) % 2 == 0 {
                    *pixel = image::Rgb([16, 16, 16]);
                } else {
                    bright += 1;
                    *pixel = image::Rgb([239, 239, 239]);
                }
            }
            mean_lumi = 255 * 3 * bright / total;
        }

        Pattern::Checkerboard(a, b) => {
            mean_lumi = 255 * 3 / 2;
            for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
                let t1 = (x / a as u32) % 2 == 0;
                let t2 = (y / b as u32) % 2 == 0;

                *pixel = if t1 == t2 { image::Rgb([16, 16, 16]) } else { image::Rgb([239, 239, 239]) };
            }
        }

        Pattern::Noise(nr, ng, nb, br, bb, bg) => {
            mean_lumi = 255 * 3 / 2; // TODO: one may argue...

            // TODO: could be faster using fill and passing a vec
            for (_x, _y, pixel) in imgbuf.enumerate_pixels_mut() {
                let on: bool = rng.gen::<u8>() > 128;
                if on {
                    *pixel = image::Rgb([nr, ng, nb]);
                } else {
                    *pixel = image::Rgb([br, bg, bb]);
                }
            }
        }

        Pattern::Grid(a, b) => {
            let mut total = 0;
            let mut bright = 0;
            for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
                total += 1;

                if x % a as u32 == 0 && y % b as u32 == 0 {
                    *pixel = image::Rgb([16, 16, 16]);
                } else {
                    bright += 1;

                    *pixel = image::Rgb([239, 239, 239]);
                }
            }
            mean_lumi = 255 * 3 * bright / total;
        }

        Pattern::Gradient(_) => {
            let mut samples: usize = 0;
            for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
                let r = (random * x as f32) as u8;
                let g = (random * 128u8 as f32) as u8 + 64u8;
                let b = (random * y as f32) as u8;

                let average_cumul = mean_lumi as usize * samples;
                let lumi = r as u16 + g as u16 + b as u16;
                mean_lumi = u16::try_from((average_cumul + (lumi as usize)) / (samples + 1))
                    .unwrap_or(128 * 3);
                samples += 1;

                *pixel = image::Rgb([r, g, b]);
            }
        }
    }

    let drawing_color = match mean_lumi {
        (0..=255) => image::Rgb([255, 255, 255]),
        (510..=765) => image::Rgb([0, 0, 0]),
        _ => image::Rgb([128, 128, 128]),
    };

    // new pass for the drawings if required
    if options.crosshair || options.border > 0 {
        for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
            if options.crosshair {
                if x == width / 2 || y == height / 2 {
                    *pixel = drawing_color; // cross air
                }
            }

            if options.border > 0 {
                let border_width = options.border as u32;
                // let margin = 5u32;
                if (x <= border_width)
                    || (x >= width - border_width)
                    || (y <= border_width)
                    || (y >= height - border_width)
                {
                    *pixel = drawing_color;
                }
            }
        }
    }

    imgbuf
}
